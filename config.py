import os
import connexion
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_bootstrap import Bootstrap

basedir = os.getcwd()
new_database = False

db_path = basedir + '/data/machine.db'
if not os.path.exists(db_path):
    open(db_path, 'w+').close()
    new_database = True

connex_app = connexion.App(__name__, specification_dir=basedir)
app = connex_app.app

# Configure the SQLAlchemy part of the app instance
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + db_path
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
bootstrap = Bootstrap(app)