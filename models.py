from datetime import datetime
from config import db, ma


class Machine(db.Model):
    __tablename__ = 'machine'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    config = db.Column(db.String(100), nullable=False)
    comment = db.Column(db.String(100), nullable=False)
    user = db.Column(db.String(50), default='-')
    timestamp = db.Column(db.String, default='-')


class MachineSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Machine
        include_fk = True
        load_instance = True
