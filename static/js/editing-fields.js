function setVisibleControlsRow(id, toEdit){
    if (toEdit){
        $("#text_name_span_"+id).hide()
        $("#text_comment_span_"+id).hide()
        $("#text_config_span_"+id).hide()
        $("#edit_name_input_span_"+id).show()
        $("#edit_comment_input_span_"+id).show()
        $("#edit_config_input_span_"+id).show()

        $("button[id*=btn_edit]").each(function () {
            $(this).hide()
        })

        $("button[id*=btn_remove]").each(function () {
            $(this).hide()
        })

        $("#btn_cancel_"+id).show()
        $("#btn_ok_"+id).show()
    }else{
        $("#text_name_span_"+id).show()
        $("#text_comment_span_"+id).show()
        $("#text_config_span_"+id).show()
        $("#edit_name_input_span_"+id).hide()
        $("#edit_comment_input_span_"+id).hide()
        $("#edit_config_input_span_"+id).hide()

        $("button[id*=btn_edit]").each(function () {
            $(this).show()
        })

        $("button[id*=btn_remove]").each(function () {
            $(this).show()
        })

        $("#btn_cancel_"+id).hide()
        $("#btn_ok_"+id).hide()
    }
}

function loadRowControls(id){
    $("#edit_name_input_"+id).val( $("#text_name_span_"+id).prop("innerText") )
    $("#edit_comment_input_"+id).val($("#text_comment_span_"+id).prop("innerText"))
    $("#edit_config_input_"+id).val( $("#text_config_span_"+id).prop("innerText") )
}

function applyRow(id){
    let name = $("#edit_name_input_"+id).val()
    let comment = $("#edit_comment_input_"+id).val()
    let config = $("#edit_config_input_"+id).val()

    $("#text_name_span_"+id).prop("innerText", name)
    $("#text_comment_span_"+id).prop("innerText", comment)
    $("#text_config_span_"+id).prop("innerText", config)

    post("/edit", {id: id, name: name, comment: comment, config: config});
}

$(document).ready(function () {
    $("button[id*=btn_edit]").each(function (i, el) {
        $(this).click(function () {
            let id = el.id.split("btn_edit_")[1]
            loadRowControls(id)
            setVisibleControlsRow(id, true)
        });
    });

    $("button[id*=btn_cancel]").each(function (i, el) {
        $(this).click(function () {
            let id = el.id.split("btn_cancel_")[1]
            setVisibleControlsRow(id, false)
        });
    });

    $("button[id*=btn_ok]").each(function (i, el) {
        $(this).click(function () {
            let id = el.id.split("btn_ok_")[1]
            setVisibleControlsRow(id, false)
            applyRow(id)
        });
    });

})