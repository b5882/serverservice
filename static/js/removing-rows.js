$(document).ready(function () {

    let currentRemoveId = ""

    $("button[id*=btn_remove]").each(function (i, el) {
        $(this).click(function () {
            currentRemoveId = el.id.split("btn_remove_")[1]
            $("#removeModal_body").text("Вы действительно хотите удалить устройство " + $("#text_name_span_"+currentRemoveId).text() + "?")
            $('#removeModal').modal('show')
        });
    });

    $('#removeModal').on('shown.bs.modal', function (e) {
        $('#removeModal_btnRemove').focus()
    })

    $('#removeModal_btnRemove').click(function () {
        post('/remove', {remove_ids: currentRemoveId})
    });

});