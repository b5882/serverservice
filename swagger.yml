swagger: "2.0"
info:
  description: Swagger file for configurate api
  version: "1.0.0"
  title: Swagger REST Article
consumes:
  - application/json
produces:
  - application/json

basePath: /api

paths:
  /machines:
    get:
      operationId: handlers.machines
      tags:
        - Machines
      summary: Read entire list of machines
      description: Read the list of machines
      responses:
        200:
          description: Successfully read machine list operation
          schema:
            type: array
            items:
              properties:
                id:
                  type: integer
                name:
                  type: string
                adc:
                  type: string
                config:
                  type: string
                user:
                  type: string
                timestamp:
                  type: string
    post:
      operationId: handlers.add
      tags:
        - Machines
      summary: Create a machine and add to the machine list
      description: Create a new machine in the machine list
      parameters:
        - name: machine
          in: body
          description: Machine to create
          required: True
          schema:
            type: object
            properties:
              name:
                type: string
                description: Machine name
              adc:
                type: string
                description: ADC type
              config:
                type: string
                description: Num of units
      responses:
        201:
          description: Successfully created machine in list
          schema:
            type: object
            properties:
              id:
                type: integer
              name:
                type: string
              adc:
                type: string
              config:
                type: string
              user:
                type: string
              timestamp:
                type: string
        409:
          description: Detailed error description
          schema:
            type: object
            properties:
              error:
                type: string

  /machines/{id}:
    delete:
      operationId: handlers.remove_item
      tags:
        - Machines
      summary: Remove a machine from the machine list
      description: Remove a machine
      parameters:
        - name: id
          in: path
          description: Id of the machine to remove
          type: integer
          required: True
      responses:
        200:
          description: Successfully deleted a machine from machine list
        404:
          description: Not found id

  /machines/set/{id}:
    put:
      operationId: handlers.set_owner
      tags:
        - Machines
      summary: Set machine owner
      description: Update machine owner
      parameters:
        - name: id
          in: path
          description: Id of the machine to update
          type: integer
          required: True
        - name: user
          in: body
          schema:
            type: object
            properties:
              user:
                type: string
      responses:
        200:
          description: Successfully updated machine owner
          schema:
            type: object
            properties:
              id:
                type: integer
              name:
                type: string
              config:
                type: string
              user:
                type: string
              timestamp:
                type: string
        401:
          description: Detailed error description
          schema:
            type: object
            properties:
              error:
                type: string
        404:
          description: Not found id

  /machines/drop/{id}:
    put:
      operationId: handlers.drop_owner
      tags:
        - Machines
      summary: Drop machine owner
      description: Update machine owner
      parameters:
        - name: id
          in: path
          description: Id of the machine to update
          type: integer
          required: True
      responses:
        200:
          description: Successfully updated machine owner
          schema:
            type: object
            properties:
              id:
                type: integer
              name:
                type: string
              config:
                type: string
              user:
                type: string
              timestamp:
                type: string
        401:
          description: Detailed error description
          schema:
            type: object
            properties:
              error:
                type: string
        404:
          description: Not found id

  /machines/edit:
    post:
      operationId: handlers.edit
      tags:
        - Machines
      summary: Edit a machine without changing the owner and time
      description: Edit a machine without changing the owner and time
      parameters:
        - name: machine
          in: body
          description: Machine to edit
          required: True
          schema:
            type: object
            properties:
              id:
                type: string
                description: Machine id
              name:
                type: string
                description: Machine name
              adc:
                type: string
                description: ADC type
              config:
                type: string
                description: Num of units
      responses:
        201:
          description: Successfully edited machine
          schema:
            type: object
            properties:
              id:
                type: integer
              name:
                type: string
              adc:
                type: string
              config:
                type: string
              user:
                type: string
              timestamp:
                type: string
        404:
          description: Not found id