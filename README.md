Create docker image:

NOTE: I know that -p key drops when we use --network=host. This is temporary, I'll figure out how to link containers in a separate network

1. ```docker build -t bposserver/alpine```
2. ```docker run -d -p 5000:5000 -v ~/serv/data:/app/data --restart always --network host bposserver/alpine```

If you want to change used port, for example you want to use 9999 port, you must swap key ```-p 5000:50000``` to ```-p 9999:9999``` to docker run command. Also you should give argument to docker run '9999'. And your docker run comman will must looks like:
```docker run -d -p 9999:9999 -v ~/serv/data:/app/data --restart always --network host bposserver/alpine '9999'```

If you have old database you can move it to mount path(on my example to ~/serv/data). You must did it before start container.

I recomend use docker compose to deploy bot + server containers.