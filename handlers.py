import datetime

from config import db
from models import Machine, MachineSchema


def machines():
    machine_list = Machine.query.order_by(Machine.id).all()
    schema = MachineSchema(many=True)
    return schema.dump(machine_list)


def edit(machine):
    m_id = machine.get('id')
    name = machine.get('name')
    comment = machine.get('comment')
    config = machine.get('config')
    machine = Machine.query.filter(Machine.id == m_id).one_or_none()

    if machine is not None:
        machine.name = name
        machine.comment = comment
        machine.config = config
        db.session.merge(machine)
        db.session.commit()
        schema = MachineSchema()
        return schema.dump(machine), 201

    return {'response': f'Id {m_id} not found!'}, 404


def add(machine):
    name = machine['name']
    exist = Machine.query.filter(Machine.name == name).one_or_none()

    if exist is None:
        schema = MachineSchema()
        new_machine = schema.load(machine, session=db.session)
        db.session.add(new_machine)
        db.session.commit()
        return schema.dump(new_machine), 201

    return {'response': f'Machine {name} already exists!'}, 409


def set_owner(id, user):
    machine = Machine.query.filter(Machine.id == id).one_or_none()
    if machine is not None:
        machine.user = user.get('user')
        machine.timestamp = datetime.datetime.now().strftime("%H:%M, %d.%m")
        db.session.merge(machine)
        db.session.commit()
        schema = MachineSchema()
        return schema.dump(machine), 200

    return {'response': f'Id {id} not found!'}, 404


def drop_owner(id):
    machine = Machine.query.filter(Machine.id == id).one_or_none()
    if machine is not None:
        machine.user = '-'
        machine.timestamp = '-'
        db.session.merge(machine)
        db.session.commit()
        schema = MachineSchema()
        return schema.dump(machine), 200

    return {'response': f'Id {id} not found!'}, 404


def remove_item(id):
    responce, code = remove(id)
    if code != 404:
        update_ids(id)

    return responce, code


def remove_items(ids: list):
    out_list = []
    for id in ids:
        responce, code = remove(id)
        out_list.append([id, responce, code])

    ids.sort()
    update_ids(ids[0])
    return out_list


def remove(id):
    machine = Machine.query.filter(Machine.id == id).one_or_none()
    if machine is not None:
        db.session.delete(machine)
        db.session.commit()
        return {'response': f'Id {id} deleted!'}, 200

    return {'response': f'Id {id} not found!'}, 404


def update_ids(start_id):
    machine_list = Machine.query.filter(Machine.id > start_id).\
        order_by(Machine.id).all()

    init_id = int(start_id)
    for machine in machine_list:
        machine.id = init_id
        init_id += 1

    db.session.commit()
