from flask import render_template, request, redirect
from sys import argv
import config
from config import db
from handlers import machines, add, remove_items, edit

connex_app = config.connex_app
connex_app.add_api('swagger.yml')

if config.new_database:
    db.create_all()


@connex_app.route('/edit', methods=['POST'])
def edit_machine():
    form = request.form.to_dict()
    if form['id'] != '':
        response, code = edit(form)
        if code != 201:
            return response

    return redirect('/')


@connex_app.route('/add', methods=['POST'])
def add_machine():
    form = request.form.to_dict()
    if form['name'] != '':
        response, code = add(form)
        if code != 201:
            return response

    return redirect('/')


@connex_app.route('/remove', methods=['POST'])
def remove_machine():
    form = request.form.to_dict()
    if len(form['remove_ids']) > 0:
        ids = form['remove_ids'].split(',')
        remove_items(ids)

    return redirect('/')


@connex_app.route('/', methods=['GET', 'POST'])
def home():
    return render_template('index.html', elements=machines())


if __name__ == '__main__':
    port = 5000 if len(argv) < 2 else int(argv[1])
    connex_app.run(host='0.0.0.0', port=port, debug=False)
